#include "Camera.hpp"
#include<iostream>

namespace gps {

    //Camera constructor
    Camera::Camera(glm::vec3 cameraPosition, glm::vec3 cameraTarget, glm::vec3 cameraUp) {
        this->cameraPosition = cameraPosition;
        this->cameraTarget = cameraTarget;
        this->cameraUpDirection = cameraUp;
        this->cameraFrontDirection = glm::normalize(cameraTarget - cameraPosition);
        this->cameraRightDirection = glm::cross(cameraFrontDirection, cameraUp);
        //TODO - Update the rest of camera parameters
        this->yawAngle = 0;
        this->pitchAngle = 0;

    }

    //return the view matrix, using the glm::lookAt() function
    glm::mat4 Camera::getViewMatrix() {
        return glm::lookAt(cameraPosition, cameraTarget, cameraUpDirection);
        
    }

    //update the camera internal parameters following a camera move event
    void Camera::move(MOVE_DIRECTION direction, float speed) {
        switch (direction) {
        case MOVE_FORWARD:
            cameraPosition += cameraFrontDirection * speed;
            cameraTarget += cameraFrontDirection * speed;
            break;
        case MOVE_LEFT:
            cameraPosition -= cameraRightDirection * speed;
            cameraTarget -= cameraRightDirection * speed;
            break;
        case MOVE_RIGHT:
            cameraPosition += cameraRightDirection * speed;
            cameraTarget += cameraRightDirection * speed;
            break;
        case MOVE_BACKWARD:
            cameraPosition -= cameraFrontDirection * speed;
            cameraTarget -= cameraFrontDirection * speed;
            break;
        }
    }

    //update the camera internal parameters following a camera rotate event
    //yaw - camera rotation around the y axis
    //pitch - camera rotation around the x axis
    void Camera::rotate(float pitch, float yaw) {
        
        yawAngle += yaw;
        pitchAngle += pitch;

   
      

        //rotatie in plan orizontal

        cameraTarget.z = cameraPosition.z - cos(glm::radians(yawAngle)); //unghiul intern al camerei modificat in urma rotatiei
        cameraTarget.x = cameraPosition.x + sin(glm::radians(yawAngle));

        //rotatie in plan vertical
        cameraTarget.y = cameraPosition.y - sin(glm::radians(pitchAngle));

        //daca misc camera target pe perimetrul cercului => rotesc camera

        //vectorii se misca odata cu camera

        //front direction
        this->cameraFrontDirection = glm::normalize(this->cameraTarget - this->cameraPosition);
        //right direction
        this->cameraRightDirection = glm::normalize(glm::cross(this->cameraFrontDirection , this->cameraUpDirection));



    }
}