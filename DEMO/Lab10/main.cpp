//
//  main.cpp
//  OpenGL Advances Lighting
//
//  Created by CGIS on 28/11/16.
//  Copyright � 2016 CGIS. All rights reserved.
//

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/matrix_inverse.hpp"
#include "glm/gtc/type_ptr.hpp"

#include "Shader.hpp"
#include "Model3D.hpp"
#include "Camera.hpp"

#include <iostream>
#include "SkyBox.hpp"
#include <random>

int glWindowWidth = 800;
int glWindowHeight = 600;
int retina_width, retina_height;
GLFWwindow* glWindow = NULL;

const unsigned int SHADOW_WIDTH = 2048;
const unsigned int SHADOW_HEIGHT = 2048;

glm::mat4 model;
GLuint modelLoc;
glm::mat4 view;
GLuint viewLoc;
glm::mat4 projection;
GLuint projectionLoc;
glm::mat3 normalMatrix;
GLuint normalMatrixLoc;
glm::mat4 lightRotation;

glm::vec3 lightDir;
GLuint lightDirLoc;
glm::vec3 lightColor;
GLuint lightColorLoc;

GLuint modelLoc2;
GLuint viewLoc2;
GLuint projectionLoc2;
GLuint normalMatrixLoc2;
GLuint lightDirLoc2;

gps::Camera myCamera(
				glm::vec3(0.0f, 1.0f, 4.0f), //camera position - unde este camera pozitionata
				//modificand Z, apropiem sau indepartam camera(Z mic = obiecte mari, Z mare = obiecte mici)
				glm::vec3(0.0f, 0.0f, 0.0f), //camera target - spre ce pointeaza camera
				glm::vec3(0.0f, 1.0f, 0.0f)); //camera up - vector are pointeaza spre Y pozitiv al camerei



bool pressedKeys[1024];
float angleY = 0.0f;
GLfloat lightAngle;

gps::Model3D nanosuit;
gps::Model3D ground;
gps::Model3D lightCube;
gps::Model3D screenQuad;
gps::SkyBox mySkyBox;

gps::Model3D scena;
gps::Model3D wolf2;
gps::Model3D moon;
gps::Model3D lantern;
gps::Model3D cloud;
gps::Model3D raindrop1;
gps::Model3D raindrop2;

gps::Shader myCustomShader;
gps::Shader lightShader;
gps::Shader screenQuadShader;
gps::Shader depthMapShader;
gps::Shader skyboxShader;
gps::Shader fogShader;


GLuint shadowMapFBO;

GLuint depthMapTexture;

bool showDepthMap;
bool showFog;

std::vector<const GLchar*> faces;

float xpos0,ypos0= 800.0f / 2.0;
bool begin=true;
float cameraSpeed = 0.1f;
float mouseSpeed = 0.08f;
float rotateWolf = 0;

float raindropHeight = 1.0f;

glm::vec3 lanternPos = glm::vec3(0.1,0.5,0);
glm::vec3 raindropPos = glm::vec3(0, 0.5, 0);


glm::vec3 auxColor = glm::vec3(1.0f, 1.0f, 1.0f); //white color

glm::vec3 scaleValue;

GLuint textureID;

std::vector<glm::vec3> rainVec;
bool showRain;

//GLuint textureID;
//glGenTextures(1, &textureID);glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

GLenum glCheckError_(const char *file, int line) {
	GLenum errorCode;
	while ((errorCode = glGetError()) != GL_NO_ERROR)
	{
		std::string error;
		switch (errorCode)
		{
		case GL_INVALID_ENUM:                  error = "INVALID_ENUM"; break;
		case GL_INVALID_VALUE:                 error = "INVALID_VALUE"; break;
		case GL_INVALID_OPERATION:             error = "INVALID_OPERATION"; break;
		case GL_STACK_OVERFLOW:                error = "STACK_OVERFLOW"; break;
		case GL_STACK_UNDERFLOW:               error = "STACK_UNDERFLOW"; break;
		case GL_OUT_OF_MEMORY:                 error = "OUT_OF_MEMORY"; break;
		case GL_INVALID_FRAMEBUFFER_OPERATION: error = "INVALID_FRAMEBUFFER_OPERATION"; break;
		}
		std::cout << error << " | " << file << " (" << line << ")" << std::endl;
	}
	return errorCode;
}
#define glCheckError() glCheckError_(__FILE__, __LINE__)


void windowResizeCallback(GLFWwindow* window, int width, int height) {
	
	//pentru dimensiune in coord. ecran
	glfwGetWindowSize(glWindow, &glWindowWidth, &glWindowHeight);


	fprintf(stdout, "window resized to width: %d , and height: %d\n", glWindowWidth, glWindowHeight);

	//pentru afisare continut

	//pentru pixeli
	glfwGetFramebufferSize(glWindow, &glWindowWidth, &glWindowHeight);

	//in pixeli
	glViewport(0, 0, glWindowWidth, glWindowHeight);

}

void keyboardCallback(GLFWwindow* window, int key, int scancode, int action, int mode) {
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	if (key == GLFW_KEY_M && action == GLFW_PRESS)
		showDepthMap = !showDepthMap;

	//ca sa afisam ceata
	if (key == GLFW_KEY_F && action == GLFW_PRESS)
		showFog = !showFog;
	
	//ca sa afisam ploaia
	if (key == GLFW_KEY_Z && action == GLFW_PRESS)
		showRain = !showRain;

	if (key >= 0 && key < 1024)
	{
		if (action == GLFW_PRESS)
			pressedKeys[key] = true;
		else if (action == GLFW_RELEASE)
			pressedKeys[key] = false;
	}
}

void mouseCallback(GLFWwindow* window, double xpos, double ypos) {

	if (begin)
	{
		xpos0 = xpos;
		ypos0 = ypos;
		begin = false;
	}
	else
	{
		double deltax = xpos - xpos0;
		double deltay = ypos - ypos0;

		myCamera.rotate(-1*deltay*mouseSpeed, deltax *mouseSpeed);

		xpos0 = xpos;
		ypos0 = ypos;
	}

}


void changeSceneColor()
{
	auxColor.x += 0.1f;
	auxColor.y += 0.1f;
	auxColor.z += 0.1f;
	
	lightColor=auxColor;
	myCustomShader.useShaderProgram();
	lightColorLoc = glGetUniformLocation(myCustomShader.shaderProgram, "lightColor");
	glUniform3fv(lightColorLoc, 1, glm::value_ptr(lightColor));
}

void reverseSceneColor()
{
	auxColor.x -= 0.1f;
	auxColor.y -= 0.1f;
	auxColor.z -= 0.1f;

	lightColor = auxColor;
	myCustomShader.useShaderProgram();
	lightColorLoc = glGetUniformLocation(myCustomShader.shaderProgram, "lightColor");
	glUniform3fv(lightColorLoc, 1, glm::value_ptr(lightColor));
}

void checkCollision(GLfloat xobj1, GLfloat yobj1, GLfloat xobj2, GLfloat yobj2)
{
	if (xobj1 == xobj2 || yobj1 == yobj2)
	{
		std::cout << "am detectat coliziune " << std::endl;
	}
}

void createRain(gps::Shader shader, bool depthPass)
{
	for (int i = 0; i < 20; i++)
	{
		//desenam in pozitii random stropii de ploaie, apoi vom translata
		float x = (rand() % (5 - (-2)) + (-2)); //numere intre 0-5
		float z = (rand() % (3 - (-2)) + (-2));

		glm::vec3 drop = glm::vec3(x, raindropHeight, z);

		//adaugam in vector
		rainVec.push_back(drop);
	}

	for (int i = 0; i < 20; i++)
	{
		model = glm::mat4(1.0f);
		//raindropPos.y = raindropHeight;
		//model = glm::translate(model, raindropPos);
		rainVec[i].y = raindropHeight;

		model = glm::translate(model, rainVec[i]);

		glUniformMatrix4fv(glGetUniformLocation(shader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));

		// do not send the normal matrix if we are rendering in the depth map
		if (!depthPass) {
			normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
			glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
		}

		raindrop1.Draw(shader);
	}
	
}

void processMovement()
{
	//rotire scena principala
	if (pressedKeys[GLFW_KEY_Q]) {
		angleY -= 1.0f;
	}

	if (pressedKeys[GLFW_KEY_E]) {
		angleY += 1.0f;
	}


	//pentru pozitia lanternei care este un fel de sursa de lumina

	if (pressedKeys[GLFW_KEY_UP]) {
		
		lanternPos.z -= 0.1;
		raindropPos.z -= 0.1;
	}

	if (pressedKeys[GLFW_KEY_DOWN]) {

		lanternPos.z += 0.1;
		raindropPos.z += 0.1;
	}

	if (pressedKeys[GLFW_KEY_LEFT]) {

		lanternPos.x -= 0.1;
		raindropPos.x -= 0.1;
		
	}

	if (pressedKeys[GLFW_KEY_RIGHT]) {

		lanternPos.x += 0.1;
		raindropPos.x += 0.1;
	}

	//miscarea camerei
	if (pressedKeys[GLFW_KEY_W]) {
		myCamera.move(gps::MOVE_FORWARD, cameraSpeed);

	}

	if (pressedKeys[GLFW_KEY_S]) {
		myCamera.move(gps::MOVE_BACKWARD, cameraSpeed);		
	}

	if (pressedKeys[GLFW_KEY_A]) {
		myCamera.move(gps::MOVE_LEFT, cameraSpeed);		
	}

	if (pressedKeys[GLFW_KEY_D]) {
		myCamera.move(gps::MOVE_RIGHT, cameraSpeed);		
	}

	//solid
	if (glfwGetKey(glWindow, GLFW_KEY_B)) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	}

	//wireframe
	if (glfwGetKey(glWindow, GLFW_KEY_V)) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}

	//point
	if (glfwGetKey(glWindow, GLFW_KEY_N)) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
	}

	//pentru miscarea lupului mare
	if (pressedKeys[GLFW_KEY_P]) {
		rotateWolf += 0.2;
	}


	//pentru schimbarea culorii scenei
	if (pressedKeys[GLFW_KEY_X]) {
		changeSceneColor();
	}

	if (pressedKeys[GLFW_KEY_Y]) {
		reverseSceneColor();
	}

	//scalare + lup
	if (pressedKeys[GLFW_KEY_G]) {
		scaleValue += 0.05;
	}

	if (pressedKeys[GLFW_KEY_H]) {
		scaleValue -= 0.05;
	}
}


bool initOpenGLWindow()
{
	if (!glfwInit()) {
		fprintf(stderr, "ERROR: could not start GLFW3\n");
		return false;
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_SRGB_CAPABLE, GLFW_TRUE);
	glfwWindowHint(GLFW_SAMPLES, 4);

	glWindow = glfwCreateWindow(glWindowWidth, glWindowHeight, "OpenGL Shader Example", NULL, NULL);
	if (!glWindow) {
		fprintf(stderr, "ERROR: could not open window with GLFW3\n");
		glfwTerminate();
		return false;
	}

	glfwSetWindowSizeCallback(glWindow, windowResizeCallback); //cand modificam dimensiunea ferestrei
	glfwSetKeyCallback(glWindow, keyboardCallback);
	glfwSetCursorPosCallback(glWindow, mouseCallback);
	//glfwSetInputMode(glWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	glfwMakeContextCurrent(glWindow);

	glfwSwapInterval(1);

	// start GLEW extension handler
	glewExperimental = GL_TRUE;
	glewInit();

	// get version info
	const GLubyte* renderer = glGetString(GL_RENDERER); // get renderer string
	const GLubyte* version = glGetString(GL_VERSION); // version as a string
	printf("Renderer: %s\n", renderer);
	printf("OpenGL version supported %s\n", version);

	//for RETINA display
	//glfwGetFramebufferSize(glWindow, &retina_width, &retina_height);
	glfwGetFramebufferSize(glWindow, &glWindowWidth, &glWindowHeight);
	

	return true;
}

void initOpenGLState()
{
	//glClearColor(0.3, 0.3, 0.3, 1.0);
	glClearColor(0.5, 0.5, 0.5, 1.0);

	glViewport(0, 0, glWindowWidth, glWindowHeight);

	glEnable(GL_DEPTH_TEST); // enable depth-testing
	glDepthFunc(GL_LESS); // depth-testing interprets a smaller value as "closer"
	glEnable(GL_CULL_FACE); // cull face
	glCullFace(GL_BACK); // cull back face
	glFrontFace(GL_CCW); // GL_CCW for counter clock-wise

	glEnable(GL_FRAMEBUFFER_SRGB);
}

void initObjects() {
	//ground.LoadModel("objects/ground/ground.obj");
	lightCube.LoadModel("objects/cube/cube.obj");
	screenQuad.LoadModel("objects/quad/quad.obj");
	scena.LoadModel("objects/scene/scena.obj");
	wolf2.LoadModel("objects/scene/wolf2.obj");
	moon.LoadModel("objects/moon/moon.obj");
	lantern.LoadModel("objects/lantern/lantern.obj");
	cloud.LoadModel("objects/rain/cloud.obj");
	raindrop1.LoadModel("objects/rain/raindrop.obj");
}

void initShaders() {
	myCustomShader.loadShader("shaders/shaderStart.vert", "shaders/shaderStart.frag");
	myCustomShader.useShaderProgram();

	//shader pentru ceata
	fogShader.loadShader("shaders/fogShader.vert", "shaders/fogShader.frag");
	fogShader.useShaderProgram();

	//luna e sursa de lumina
	lightShader.loadShader("shaders/lightCube.vert", "shaders/lightCube.frag");
	lightShader.useShaderProgram();

	screenQuadShader.loadShader("shaders/screenQuad.vert", "shaders/screenQuad.frag");
	screenQuadShader.useShaderProgram();

	depthMapShader.loadShader("shaders/depthMapShader.vert", "shaders/depthMapShader.frag");
	depthMapShader.useShaderProgram();

	skyboxShader.loadShader("shaders/skyboxShader.vert", "shaders/skyboxShader.frag");
	skyboxShader.useShaderProgram();

	
}

void initUniforms() {
	myCustomShader.useShaderProgram();

	model = glm::mat4(1.0f);
	modelLoc = glGetUniformLocation(myCustomShader.shaderProgram, "model");
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

	view = myCamera.getViewMatrix();
	viewLoc = glGetUniformLocation(myCustomShader.shaderProgram, "view");
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
	
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	normalMatrixLoc = glGetUniformLocation(myCustomShader.shaderProgram, "normalMatrix");
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	
	projection = glm::perspective(glm::radians(45.0f), (float)glWindowWidth / (float)glWindowHeight, 0.1f, 1000.0f);
	projectionLoc = glGetUniformLocation(myCustomShader.shaderProgram, "projection");
	glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));

	//set the light direction (direction towards the light)
	lightDir = glm::vec3(0.0f, 2.5f, 1.0f);


	lightRotation = glm::rotate(glm::mat4(1.0f), glm::radians(lightAngle), glm::vec3(0.0f, 1.0f, 0.0f));
	lightDirLoc = glGetUniformLocation(myCustomShader.shaderProgram, "lightDir");	
	glUniform3fv(lightDirLoc, 1, glm::value_ptr(glm::inverseTranspose(glm::mat3(view * lightRotation)) * lightDir));
	

	//set light color
	lightColor = glm::vec3(1.0f, 1.0f, 1.0f); //white light
	lightColorLoc = glGetUniformLocation(myCustomShader.shaderProgram, "lightColor");
	glUniform3fv(lightColorLoc, 1, glm::value_ptr(lightColor));

	//=========================================================================================

	fogShader.useShaderProgram();

	modelLoc2 = glGetUniformLocation(fogShader.shaderProgram, "model");
	glUniformMatrix4fv(modelLoc2, 1, GL_FALSE, glm::value_ptr(model));
	viewLoc2 = glGetUniformLocation(fogShader.shaderProgram, "view");
	glUniformMatrix4fv(viewLoc2, 1, GL_FALSE, glm::value_ptr(view));

	normalMatrixLoc2 = glGetUniformLocation(fogShader.shaderProgram, "normalMatrix");
	glUniformMatrix3fv(normalMatrixLoc2, 1, GL_FALSE, glm::value_ptr(normalMatrix));

	projectionLoc2 = glGetUniformLocation(fogShader.shaderProgram, "projection");
	glUniformMatrix4fv(projectionLoc2, 1, GL_FALSE, glm::value_ptr(projection));

	//set the light direction (direction towards the light)
	lightDirLoc2 = glGetUniformLocation(fogShader.shaderProgram, "lightDir");
	glUniform3fv(lightDirLoc2, 1, glm::value_ptr(glm::inverseTranspose(glm::mat3(view * lightRotation)) * lightDir));
	//

	//set light color
	lightColor = glm::vec3(1.0f, 1.0f, 1.0f); //white light
	lightColorLoc = glGetUniformLocation(fogShader.shaderProgram, "lightColor");
	glUniform3fv(lightColorLoc, 1, glm::value_ptr(lightColor));

	lightShader.useShaderProgram();
	glUniformMatrix4fv(glGetUniformLocation(lightShader.shaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projection));

	mySkyBox.Load(faces);
}

void initFBO() {
	//TODO - Create the FBO, the depth texture and attach the depth texture to the FBO
	//generate FBO ID
	glGenFramebuffers(1, &shadowMapFBO);

	//create depth texture for FBO
	glGenTextures(1, &depthMapTexture);
	glBindTexture(GL_TEXTURE_2D, depthMapTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT,
		SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

	//attach texture to FBO
	glBindFramebuffer(GL_FRAMEBUFFER, shadowMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMapTexture,0);

	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);



}

glm::mat4 computeLightSpaceTrMatrix() {
	//TODO - Return the light-space transformation matrix

	glm::mat4 lightView = glm::lookAt(glm::inverseTranspose(glm::mat3(view * lightRotation)) * lightDir, glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));

	const GLfloat near_plane = 0.1f, far_plane = 5.0f;
	glm::mat4 lightProjection = glm::ortho(-1.0f, 1.0f, -1.0f, 1.0f, near_plane, far_plane);

	glm::mat4 lightSpaceTrMatrix = lightProjection * lightView;
	

	return lightSpaceTrMatrix;

}

void drawObjects(gps::Shader shader, bool depthPass) {
		
	shader.useShaderProgram();
	
	//------------------SCENA MARE
	model = glm::rotate(glm::mat4(1.0f), glm::radians(angleY), glm::vec3(0.0f, 1.0f, 0.0f));
	glUniformMatrix4fv(glGetUniformLocation(shader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));

	// do not send the normal matrix if we are rendering in the depth map
	if (!depthPass) {
		normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
		glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	}

	scena.Draw(shader);

	//------------------LUPUL CARE SE ROTESTE SI ARE UMBRA
	model = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.0f));
	model = glm::rotate(glm::mat4(1.0f), glm::radians(rotateWolf), glm::vec3(2.0f, 0.0f, 0.0f));

	//se roteste pe axa Y
	//model = glm::rotate(glm::mat4(1.0f), glm::radians(rotateWolf), glm::vec3(0.0f, 1.0f, 0.0f));

	//model = glm::scale(model, scaleValue);
	glUniformMatrix4fv(glGetUniformLocation(shader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));

	// do not send the normal matrix if we are rendering in the depth map
	if (!depthPass) {
		normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
		glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	}

	wolf2.Draw(shader);

	//------------------NORUL - scena ploii
	model = glm::mat4(1.0f);
	model = glm::translate(model, lanternPos);
	
	model = glm::rotate(model, glm::radians(rotateWolf), glm::vec3(2.0f, 0.0f, 0.0f));

	//se roteste pe axa Y
	//model = glm::rotate(glm::mat4(1.0f), glm::radians(rotateWolf), glm::vec3(0.0f, 1.0f, 0.0f));

	model = glm::scale(model, scaleValue);
	glUniformMatrix4fv(glGetUniformLocation(shader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));

	// do not send the normal matrix if we are rendering in the depth map
	if (!depthPass) {
		normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
		glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	}

	cloud.Draw(shader);

	//am nevoie de pozitia obiectelor 

	//checkCollision();


	//------------------STROPII DE PLOAIE

	if (showRain)
	{
		createRain(shader, depthPass);
	}
	
	
	/*
	model = glm::mat4(1.0f);
	raindropPos.y = raindropHeight;
	model = glm::translate(model, raindropPos);
	model = glm::scale(model, scaleValue);
	//model = glm::translate(model, lanternPos);

	*/

	


}



void renderScene() {

	// depth maps creation pass
	//TODO - Send the light-space transformation matrix to the depth map creation shader and
	//		 render the scene in the depth map

	depthMapShader.useShaderProgram();
	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "lightSpaceTrMatrix"),1,GL_FALSE,glm::value_ptr(computeLightSpaceTrMatrix()));
	glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
	glBindFramebuffer(GL_FRAMEBUFFER, shadowMapFBO);
	glClear(GL_DEPTH_BUFFER_BIT);

	drawObjects(depthMapShader, true);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	// render depth map on screen - toggled with the M key 

	if (showDepthMap) {
		glViewport(0, 0, glWindowWidth, glWindowHeight);

		glClear(GL_COLOR_BUFFER_BIT);

		screenQuadShader.useShaderProgram();

		//bind the depth map
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, depthMapTexture);
		glUniform1i(glGetUniformLocation(screenQuadShader.shaderProgram, "depthMap"), 0);

		glDisable(GL_DEPTH_TEST);
		screenQuad.Draw(screenQuadShader);
		glEnable(GL_DEPTH_TEST);
	}
	else if (showFog)
	{
		// final scene rendering pass (with shadows)

		glViewport(0, 0, glWindowWidth, glWindowHeight);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		fogShader.useShaderProgram();

		view = myCamera.getViewMatrix();
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));

		lightRotation = glm::rotate(glm::mat4(1.0f), glm::radians(lightAngle), glm::vec3(0.0f, 1.0f, 0.0f));
		glUniform3fv(lightDirLoc, 1, glm::value_ptr(glm::inverseTranspose(glm::mat3(view * lightRotation)) * lightDir));
		glm::inverseTranspose(glm::mat3(view * lightRotation))* lightDir;

		//bind the shadow map
		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D, depthMapTexture);
		glUniform1i(glGetUniformLocation(fogShader.shaderProgram, "shadowMap"), 3);

		glUniformMatrix4fv(glGetUniformLocation(fogShader.shaderProgram, "lightSpaceTrMatrix"),
			1,
			GL_FALSE,
			glm::value_ptr(computeLightSpaceTrMatrix()));

		drawObjects(fogShader, false);

		//draw a white moon around the light

		lightShader.useShaderProgram();
		fogShader.useShaderProgram();

		glUniformMatrix4fv(glGetUniformLocation(lightShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
		glUniformMatrix4fv(glGetUniformLocation(fogShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));


		model = lightRotation;
		model = glm::translate(model, 1.0f * lightDir);

		//luna se roteste incontinuu, simetric cu pamantul, "arata aceeasi fata"
		//model = glm::rotate(model, glm::radians(rotateMoon), glm::vec3(0.0f, 2.0f, 0.0f));

		model = glm::scale(model, glm::vec3(0.1f));

		glUniformMatrix4fv(glGetUniformLocation(lightShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
		glUniformMatrix4fv(glGetUniformLocation(fogShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));

		//facem o rotire contnua a lunii
		lightAngle -= 0.5f;

		moon.Draw(lightShader);
		moon.Draw(fogShader);




		//-------------------LANTERNA
		model = glm::translate(glm::mat4(1.0f), lanternPos);

		//se roteste pe axa Y
		//model = glm::rotate(glm::mat4(1.0f), glm::radians(rotateWolf), glm::vec3(0.0f, 1.0f, 0.0f));

		//model = glm::scale(model, glm::vec3(0.1f));
		glUniformMatrix4fv(glGetUniformLocation(lightShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
		glUniformMatrix4fv(glGetUniformLocation(fogShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));


		//lantern.Draw(lightShader);
		//lantern.Draw(myCustomShader);
		mySkyBox.Draw(skyboxShader, view, projection);



		if (raindropHeight > 0.0f)
		{
			raindropHeight -= 0.05;
		}
		else
		{
			raindropHeight = 2.0f;
		}


	}
	else if (!showFog) {

		// final scene rendering pass (with shadows)

		glViewport(0, 0, glWindowWidth, glWindowHeight);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		myCustomShader.useShaderProgram();

		view = myCamera.getViewMatrix();
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
				
		lightRotation = glm::rotate(glm::mat4(1.0f), glm::radians(lightAngle), glm::vec3(0.0f, 1.0f, 0.0f));
		glUniform3fv(lightDirLoc, 1, glm::value_ptr(glm::inverseTranspose(glm::mat3(view * lightRotation)) * lightDir));
		glm::inverseTranspose(glm::mat3(view * lightRotation))* lightDir;

		//bind the shadow map
		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D, depthMapTexture);
		glUniform1i(glGetUniformLocation(myCustomShader.shaderProgram, "shadowMap"), 3);

		glUniformMatrix4fv(glGetUniformLocation(myCustomShader.shaderProgram, "lightSpaceTrMatrix"),
			1,
			GL_FALSE,
			glm::value_ptr(computeLightSpaceTrMatrix()));

		drawObjects(myCustomShader, false);

		//draw a white moon around the light

		lightShader.useShaderProgram();
		myCustomShader.useShaderProgram();

		glUniformMatrix4fv(glGetUniformLocation(lightShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
		glUniformMatrix4fv(glGetUniformLocation(myCustomShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));


		model = lightRotation;
		model = glm::translate(model, 1.0f * lightDir);

		//luna se roteste incontinuu, simetric cu pamantul, "arata aceeasi fata"
		//model = glm::rotate(model, glm::radians(rotateMoon), glm::vec3(0.0f, 2.0f, 0.0f));

		model = glm::scale(model, glm::vec3(0.1f));

		glUniformMatrix4fv(glGetUniformLocation(lightShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
		glUniformMatrix4fv(glGetUniformLocation(myCustomShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));

		//facem o rotire contnua a lunii
		lightAngle -= 0.5f;

		moon.Draw(lightShader);
		moon.Draw(myCustomShader);
		



		//-------------------LANTERNA
		model = glm::translate(glm::mat4(1.0f), lanternPos);

		//se roteste pe axa Y
		//model = glm::rotate(glm::mat4(1.0f), glm::radians(rotateWolf), glm::vec3(0.0f, 1.0f, 0.0f));

		//model = glm::scale(model, glm::vec3(0.1f));
		glUniformMatrix4fv(glGetUniformLocation(lightShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
		glUniformMatrix4fv(glGetUniformLocation(myCustomShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));


		//lantern.Draw(lightShader);
		//lantern.Draw(myCustomShader);
		mySkyBox.Draw(skyboxShader, view, projection);

		

		if (raindropHeight > 0.0f)
		{
			raindropHeight -= 0.05;
		}
		else
		{
			raindropHeight = 1.0f;
		}
		

		


	}
}

void cleanup() {
	glDeleteTextures(1,& depthMapTexture);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glDeleteFramebuffers(1, &shadowMapFBO);
	glfwDestroyWindow(glWindow);
	//close GL context and any other GLFW resources
	glfwTerminate();
}

int main(int argc, const char * argv[]) {

	if (!initOpenGLWindow()) {
		glfwTerminate();
		return 1;
	}
	

	
	/*
	faces.push_back("textures/skybox/right.tga");
	faces.push_back("textures/skybox/left.tga");
	faces.push_back("textures/skybox/top.tga");
	faces.push_back("textures/skybox/bottom.tga");
	faces.push_back("textures/skybox/back.tga");
	faces.push_back("textures/skybox/front.tga");
	*/

	faces.push_back("textures/skybox/negx.jpg");
	faces.push_back("textures/skybox/negy.jpg");
	faces.push_back("textures/skybox/negz.jpg");
	faces.push_back("textures/skybox/posx.jpg");
	faces.push_back("textures/skybox/posy.jpg");
	faces.push_back("textures/skybox/posz.jpg");
	



	initOpenGLState();
	initObjects();
	initShaders();
	initUniforms();
	initFBO();

	glCheckError();

	while (!glfwWindowShouldClose(glWindow)) {
		processMovement();
		renderScene();		

		glfwPollEvents();
		glfwSwapBuffers(glWindow);
	}

	cleanup();

	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

	return 0;
}
